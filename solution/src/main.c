#include "bmp.h"
#include "argument_parser.h"
#include "image.h"
#include "rotate.h"
#include <errno.h>
#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define READ_FILE "r"
#define WRITE_FILE "w"

#define INPUT_FILE_ACCESS_ERROR "Can't access read file"
#define OUTPUT_FILE_ACCESS_ERROR "Can't access read file"
#define BMP_IMAGE_READ_ERROR "Can't read BMP image: "
#define BMP_IMAGE_WRITE_ERROR "Can't write BMP image: "
#define OUT_OF_MEMORY_ERROR "Out of memmory error"

static const char* const HELP_MESSAGE = "./rotate_image [path_to_img] [path_to_output] [angle_in_degrees]";

static const char* const WRITE_STATUS_MESSAGES[WRITE_COUNT] = {
  [WRITE_OK] = "Ok",
  [WRITE_ERROR] = OUTPUT_FILE_ACCESS_ERROR
};

static const char* const READ_STATUS_MESSAGES[READ_COUNT] = {
  [READ_OK] = "Ok",
  [READ_INVALID_FORMAT] = "Invalid format",
  [READ_FILE_ERROR] = INPUT_FILE_ACCESS_ERROR,
  [READ_MALLOC_ERROR] = "Out of memory"
};

static int read_and_rotate(char* fin_name, struct image* img, int32_t angle) {
  FILE* source_file = fopen(fin_name, READ_FILE);
  if (source_file == NULL) {
    perror(INPUT_FILE_ACCESS_ERROR);
    return 1;
  }

  struct image image = EMPTY_IMAGE;
  enum read_status read_status;
  if ((read_status = from_bmp(source_file, &image)) != READ_OK) {
    perror(READ_STATUS_MESSAGES[read_status]);
   fclose(source_file);
    return 1;
  }
   fclose(source_file);

  *img = rotate_by_angle(image, angle);
  return 0;
}

static int write_rotated(char* fout_name, struct image rotated) {
  FILE* output_file = fopen(fout_name, WRITE_FILE);
  if (output_file == NULL) {
    perror(OUTPUT_FILE_ACCESS_ERROR);
    return 1;
  }


  enum write_status write_status;
  if ((write_status = to_bmp(output_file, &rotated)) != WRITE_OK) {
    perror(WRITE_STATUS_MESSAGES[write_status]);
    fclose(output_file);
    free_image(rotated);
    return 1;
  }
    fclose(output_file);
    free_image(rotated);
    return 0;
}

int main( int argc, char** argv ) {

  char* fin_name;
  char* fout_name;
  int32_t angle;
  if(!validate_args(argc) || !parse_input_file(argv, &fin_name, &fout_name, &angle)) {
    perror(HELP_MESSAGE);
    return 1;
  }

  struct image rotated = EMPTY_IMAGE;
  if(read_and_rotate(fin_name, &rotated, angle)) return 1;


  return write_rotated(fout_name, rotated);
}
