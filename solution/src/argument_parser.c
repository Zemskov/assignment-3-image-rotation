#include "argument_parser.h"
#include <inttypes.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

static bool validate_angle(int32_t angle) {
  return angle % 90 == 0 && angle <= 270 && angle >= -270;
}


bool validate_args(int argc) {
  return argc == 4;
}

size_t parse_input_file(char** args, char** inputf, char** outputf, int32_t* angle) {
  char* e;
  *(angle) = (int32_t) strtol(args[3], &e, 10);
  if (!validate_angle(*angle) || strlen(e) != 0) {
    return 0;
  }
  *(inputf) = args[1];
  *(outputf) = args[2];

  return 1;
}

