#pragma once

#include <inttypes.h>
#include <stdbool.h>
#include <stdlib.h>


size_t parse_input_file(char** args, char** inputf, char** outputf, int32_t* angle);
bool validate_args(int argc);
