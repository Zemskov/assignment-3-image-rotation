#pragma once

#include <inttypes.h>
#define EMPTY_IMAGE (struct image) { 0 }

struct  __attribute__((packed)) pixel { uint8_t b, g, r; };

struct image {
  uint64_t width, height;
  struct pixel* data;
};

void free_image(struct image image);


struct image create_image(uint32_t width, uint32_t height);
