#include "bmp.h"
#include "image.h"
#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define BMP_TYPE 19778
#define BOFF_BITS 54
#define BI_PLANES 1
#define BFBIT_COUNT 24
#define BI_SIZE 40
#define BI_RGB 0
#define EMPTY_HEADER (struct bmp_header) { 0 }

const uint32_t PADDING_FILL = 0;

static size_t calculate_row_padding(const uint32_t width) {
    return (4 - (width * sizeof(struct pixel) % 4)) % 4;
}


static size_t calculate_row_size(uint32_t width) {
  return width*sizeof(struct pixel) + calculate_row_padding(width);
}


static enum read_status read_header(FILE* file, struct bmp_header* header) {
  if (fread(header, sizeof(*header), 1, file) != 1) {
    return READ_FILE_ERROR;
  }
  if (header->bfType != BMP_TYPE) {
    return READ_INVALID_FORMAT;
  }
  return READ_OK;
}

static enum read_status read_pixels(FILE* file, struct bmp_header header, struct pixel** pixels) {
  const size_t row_padding = calculate_row_padding(header.biWidth);
  *pixels = malloc(sizeof(struct pixel) * header.biWidth * header.biHeight);
  if (*pixels == NULL)
    return READ_MALLOC_ERROR;

  for (size_t i = 0; i < header.biHeight; i++) {
    if (fread(*pixels + i*header.biWidth, sizeof(struct pixel), header.biWidth, file) < header.biWidth) {
      free(*pixels);
      return READ_FILE_ERROR;
    }
    fseek(file, (long) row_padding, 1);
  }
  return READ_OK;
}

enum read_status from_bmp(FILE* file, struct image* out) {
  struct bmp_header header = EMPTY_HEADER;
  enum read_status status = read_header(file, &header);
  if(status != READ_OK) return status;
  fseek(file, header.bOffBits, 0);
  out->width = header.biWidth;
  out->height = header.biHeight;

  return read_pixels(file, header, &(out->data));
}



static enum write_status write_bmp(FILE* out, struct image const* image) {
  const size_t padding = calculate_row_padding(image->width);
  for (size_t i = 0; i < image->height; i ++)
      if (fwrite(image->data + i*image->width, sizeof(struct pixel), image->width, out) + fwrite(&PADDING_FILL, padding, 1, out) < image->width)
        return WRITE_ERROR;
  return WRITE_OK;
}

enum write_status to_bmp(FILE* out, struct image const* image) {
  const uint32_t image_size = image->height * calculate_row_size(image->width);

  const struct bmp_header header = {
    .bfType = BMP_TYPE,
    .bfileSize = sizeof(struct bmp_header) + image_size,
    .bfReserved = 0,
    .bOffBits = BOFF_BITS,
    .biSize = BI_SIZE,
    .biWidth = image->width,
    .biHeight = image->height,
    .biPlanes = BI_PLANES,
    .biBitCount = BFBIT_COUNT,
    .biCompression = BI_RGB,
    .biSizeImage = image_size,
    .biXPelsPerMeter = 0,
    .biYPelsPerMeter = 0,
    .biClrUsed = 0,
    .biClrImportant = 0
  };

  if (fwrite(&header, sizeof(header), 1, out) != 1) {
    return WRITE_ERROR;
  }

  return write_bmp(out, image);
}
