#include "image.h"
#include <errno.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>



void free_image(const struct image image) {
  if (image.data) {
    free(image.data);
  }
}


struct image create_image(uint32_t width, uint32_t height) {
    if (width <= 0 || height <= 0) return (struct image) { 0 };
    return (struct image) {
            .width = width,
            .height = height,
            .data = malloc(sizeof(struct pixel) * width * height)
    };
}

