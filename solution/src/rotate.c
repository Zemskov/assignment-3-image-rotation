#include "rotate.h"
#include "image.h"
#include <stdint.h>

static int32_t calculate_rotations(int32_t degrees) {
    return (4 - degrees / 90) % 4;
}

struct image rotate_by_angle(struct image const source, int32_t angle) {

  angle = calculate_rotations(angle);
  struct image image = source;
  for (size_t i = 0; i < angle; i++) {
        struct image rotated = rotate90(image);
        free_image(image);
        image = rotated;
    }
  return image;

}

struct image rotate90(struct image const source) {
    struct image rotatedImage = create_image(source.height, source.width);
    for (size_t i = 0; i < source.height; i++) {
        for (size_t j = 0; j < source.width; j++) {
            rotatedImage.data[source.height * j + source.height - (i + 1)] = source.data[source.width * i + j];
        }
    }
    return rotatedImage;
}
