#pragma once

#include "image.h"
#include <stddef.h>

struct image rotate90(struct image const source);

struct image rotate_by_angle(struct image const source, int32_t angle);
